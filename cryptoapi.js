const loadModule = require('./loadModule');

const path = loadModule('path');
const fs = loadModule('fs');
const crypto = loadModule('crypto');

const genKey = (passPhrase = '', keySize = 4096) => {
	const { publicKey, privateKey } = crypto.generateKeyPairSync('rsa', {
		modulusLength: keySize,
		publicKeyEncoding: {
			type: 'spki',
			format: 'pem',
		},
		privateKeyEncoding: {
			type: 'pkcs8',
			format: 'pem',
			cipher: 'aes-256-cbc',
			passphrase: passPhrase,
		},
	});
	return { publicKey, privateKey };
};

const encrypt = (toEncrypt, publicKey) => {
	if (!toEncrypt || !publicKey) {
		throw new Error(
			`Missing Argument ${
				toEncrypt ? 'publicKey (2nd argument)' : 'data (1st argument)'
			}`
		);
	}
	try {
		const absolutePath = path.resolve(publicKey);
		privateKey = fs.readFileSync(absolutePath, 'utf8');
	} catch (error) {}

	const buffer = Buffer.from(toEncrypt);
	const encrypted = crypto.publicEncrypt(publicKey, buffer);
	return encrypted.toString('base64');
};

const decrypt = (toDecrypt, privateKey, pass = '') => {
	if (!toDecrypt || !privateKey) {
		throw new Error(
			`Missing Argument ${
				toDecrypt ? 'privateKey (2nd argument)' : 'encrData (1st argument)'
			}`
		);
	}
	try {
		const absolutePath = path.resolve(privateKey);
		privateKey = fs.readFileSync(absolutePath, 'utf8');
	} catch (error) {}

	const buffer = Buffer.from(toDecrypt, 'base64');

	privateKey = crypto.createPrivateKey({
		key: privateKey,
		format: 'pem',
		passphrase: pass,
		encoding: 'utf8',
	});

	const decrypted = crypto.privateDecrypt(privateKey, buffer);

	privateKey = null;
	return decrypted.toString('utf8');
};

module.exports = {
	encrypt,
	decrypt,
	genKey,
};

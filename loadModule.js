module.exports = module => {
	let mod;
	try {
		mod = require(module);
	} catch (err) {
		throw new Error(
			`${module.substr(0, 1).toUpperCase()}${module.substr(
				1,
				module.length
			)} was not found or errored while loading!`
		);
	}
	return mod;
};

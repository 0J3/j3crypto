const console = require('console');
const { appendFileSync, writeFileSync } = require('fs');
const x = require('./cryptoapi');
const crypto = require('./loadModule')('crypto');

const oldLog = console.log;

writeFileSync('./apitest.latest.log', '');

console.log = a => {
	oldLog(a);
	appendFileSync(
		'./apitest.latest.log',
		`${a}
`
	);
};

console.log(`Test 1: Ensure Correct Decryption Results`);
console.log(`Test 1a: No Passphrase`);
let runs = 1000;

const keySize = 4096;

const a = s => {
	let t = 75;
	if (s.length > t) s = `${s.substr(0, t)}...`;
	return s;
};

const iiify = i => {
	let ii = Math.min(i, 15);

	if (keySize < 4000) {
		ii = Math.min(i, 10);
	}
	if (keySize < 2000) {
		ii = Math.min(i, 3);
	}
	if (keySize < 1000) {
		ii = Math.min(i, 1.1);
	}

	return ii;
};

for (i = 1; i <= runs; i++) {
	let ii = iiify(i);

	const { publicKey, privateKey } = x.genKey('', keySize);

	const sourceText = crypto.randomBytes(ii * 10).toString('hex');

	let encr = x.encrypt(sourceText, publicKey);

	let decr = x.decrypt(encr, privateKey);

	if (decr != sourceText) {
		throw new Error(
			`Decrypted Result (${decr}) is not the original string (${sourceText})`
		);
	} else {
		console.log(
			`[Test 1a] Success (${i}/${runs}) - Successfully en-/decrypted ${a(
				sourceText
			)} (Key Size: ${keySize})`
		);
	}
}

console.log(`Test 1b: With Passphrase`);

for (i = 1; i <= runs; i++) {
	const pass = crypto.randomBytes(10).toString('hex');

	let ii = iiify(i);

	const { publicKey, privateKey } = x.genKey(pass, keySize);

	const sourceText = crypto.randomBytes(ii * 10).toString('hex');

	let encr = x.encrypt(sourceText, publicKey);

	let decr = x.decrypt(encr, privateKey, pass);

	if (decr != sourceText) {
		throw new Error(
			`Decrypted Result (${decr}) is not the original string (${sourceText})`
		);
	} else {
		console.log(
			`[Test 1b] Success (${i}/${runs}) - Successfully en-/decrypted ${a(
				sourceText
			)} (Key Size: ${keySize} - Pass: ${pass})`
		);
	}
}

console.log(`Test 2: Invalid Keys (With Passphrase)`);

for (i = 1; i <= runs; i++) {
	const pass1 = crypto.randomBytes(10).toString('hex');
	const pass2 = crypto.randomBytes(15).toString('hex');

	let ii = iiify(i);

	const { privateKey } = x.genKey(pass1, keySize + 1); // +1 to ensure invalid keypair
	const { publicKey } = x.genKey(pass2, keySize);

	const sourceText = crypto.randomBytes(ii * 10).toString('hex');

	try {
		let encr = x.encrypt(sourceText, publicKey);

		let decr = x.decrypt(encr, privateKey, pass1);

		if (sourceText == decr) {
			console.log(
				`[Test 2] FAIL (${i}/${runs}) - Data Matches and there was no decryption error`
			);
		} else {
			console.log(`[Test 2] Success (${i}/${runs}) - Data Mismatch`);
		}
	} catch (error) {
		console.log(`[Test 2] Success (${i}/${runs}) - Decryption Error`);
	}
}

# J3Crypto

## 0J3's Crypto Wrapper

_this is an experiment, and wasn't intended for usage with important data in a production environement_

## API

(_Required Arguments are italic_ - Format: `FunctionName(Type ArgumentName = Default,Type2 ArgumentName2 = Default2) => ReturnType`)

### J3Crypto.genKey(String password = '', Int KeyLength = 4096) => String

Generates a public and private keypair with a length of `KeyLength` and the password of `password`

Example:

```js
const { publicKey, privateKey } = J3Crypto.genKey('SomePassword', 8192);
```

### J3Crypto.encrypt(String _data_, String _publicKey_) => String

Encrypts 'data' using 'publicKey'

Example:

```js
const encryptedData = J3Crypto.encrypt('SomeString', publicKey);
```

### J3Crypto.decrypt(String _encrData_, String _privateKey_, String keyPassword = '')

Decrypts 'encrData' using 'privateKey'

Example:

```js
const decryptedData = J3Crypto.decrypt(
	encryptedData,
	privateKey,
	'SomePassword'
);
```

## Tests

To run tests, clone this repo, and run `yarn test`.
The tests are available [here](https://gitlab.com/0J3/j3crypto/-/blob/main/apitest.js).
